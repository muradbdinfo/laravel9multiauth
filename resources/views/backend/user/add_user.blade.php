@extends('backend.layouts.app')
@section('content')


<section class="content">
      <div class="container-fluid">
<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">Add User User</h5>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
               
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">

                  <div class="card">
      
              <!-- /.card-header -->
              <div class="card-body">
              <form role="form" action="{{URL::to('/insert-user')}}" method="post" enctype="multipart/form-data">
                @csrf
              <div class="form-group row">
<label for="inputEmail3" class="col-sm-2 col-form-label">User Name</label>
<div class="col-sm-10">
<input type="text" class="form-control"name="name" placeholder="Full Name"required>
</div>
</div>
         
<div class="form-group row">
<label for="inputEmail3" class="col-sm-2 col-form-label">User Email</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="email" placeholder="Email" required autocomplete="off">
</div>
</div>
        

<div class="form-group row">
  <label for="inputEmail3" class="col-sm-2 col-form-label">User Password</label>
  <div class="col-sm-10">
  <input type="password"  class="form-control" name="password" placeholder="Enter Password" required>
  </div>
  </div>

  <div class="form-group row">
  <label for="inputEmail3" class="col-sm-2 col-form-label">User Type</label>
  <div class="col-sm-10">

    <select class="form-control" id="exampleFormControlSelect1" name="role" required>

    <option value="Admin">Admin</option>
    <option value="Manager">Manager</option>
    <option value="Customer">Customer</option>
   
    </select> 
  </div>
  </div>

              </div>
              <!-- /.card-body -->


<div class="card-footer">
<button type="submit" class="btn btn-info">Submit</button>
<!-- <button type="submit" class="btn btn-default float-right">Cancel</button> -->
</div>
<!-- /.card-footer -->
</form>
</div>
            <!-- /.card -->
        </div>
            </div>



</div>
                <!-- /.row -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

</div>
</div>

@endsection