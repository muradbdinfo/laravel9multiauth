<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// User Management

Route::get('/all-user', [App\Http\Controllers\Backend\UserController::class, 'AllUser'])->name('AllUser');
Route::get('/add-user', [App\Http\Controllers\Backend\UserController::class,'index']);
Route::post('/insert-user', [App\Http\Controllers\Backend\UserController::class, 'InsertUser']);
Route::get('/edit-user/{id}', [App\Http\Controllers\Backend\UserController::class, 'EditUser']);
Route::post('/update-user/{id}', [App\Http\Controllers\Backend\UserController::class, 'UpdateUser']);
Route::get('/delete-user/{id}', [App\Http\Controllers\Backend\UserController::class, 'DeleteUser']);

